<?php get_header(); ?>
  
  <!-- Blog banner
  ======================================================================= -->
  <div class="blog-banner">
      <div class="container">
          <div class="row">
              <h1><?php the_title(); ?></h1>
          </div>
      </div>
  </div>
  
  
  
  <!-- blog item in row
  ================================================================= -->
  <div class="blog-itemA">
      <div class="container">
          <div class="row">
          
         	<?php get_template_part('loop','single'); ?>

             
             <?php comments_template( '', true ); ?>
          </div>
      </div>
  </div>
  
  
  
<?php get_template_part('testimonial'); ?>
   
<?php get_footer(); ?>