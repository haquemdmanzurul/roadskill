<div id="carousel-example-generic" class="carousel slide " data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active" style="background: url(<?php echo get_template_directory_uri(); ?>/images/slider1-bg.jpg);">
          <img src="..." alt="...">
          <div class="carousel-caption">
              <h1>Reduce Operating Costs, <br> Reduce Insurance Premiums, <br> Increase Fuel Efficiency and <br> Improve Safety</h1>
              <p>- Road Skills Ltd can Show You How</p>
              <img src="<?php echo get_template_directory_uri(); ?>/images/Banner_image1.png" class="img-responsive banner1-img" alt="">
          </div>
        </div>
        <div class="item" style="background: url(<?php echo get_template_directory_uri(); ?>/images/slider2-bg.jpg);">
          <img src="..." alt="...">
          <div class="carousel-caption">
            <h1>Looking for a Fastrak Route <br> to FORS Accreditation</h1>
            <p>- Road Skills Ltd are Recognised FORS Enablers <br>  and can Help You Get There Faster</p>
            <img src="<?php echo get_template_directory_uri(); ?>/images/Banner2_image1.png" class="img-responsive banner2-img" alt="">
          </div>
        </div>

      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>