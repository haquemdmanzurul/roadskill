<?php  get_header() ?>
<!-- main slider
==================================================================== -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active" style="background: url(images/slider1-bg.jpg);">
            <img src="..." alt="...">
            <div class="carousel-caption">
                <h1>Reduce Operating Costs, <br> Reduce Insurance Premiums, <br> Increase Fuel Efficiency and <br> Improve Safety</h1>
                <p>- Road Skills Ltd can Show You How</p>
                <img src="images/Banner_image1.png" class="img-responsive banner1-img" alt="">
            </div>
        </div>
        <div class="item" style="background: url(images/slider2-bg.jpg);">
            <img src="..." alt="...">
            <div class="carousel-caption">
                <h1>Looking for a Fastrak Route <br> to FORS Accreditation</h1>
                <p>- Road Skills Ltd are Recognised FORS Enablers <br>  and can Help You Get There Faster</p>
                <img src="images/Banner2_image1.png" class="img-responsive banner2-img" alt="">
            </div>
        </div>

    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Read more about us
============================================================= -->
<div class="more-about-roadSkill">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2><b>Road Skills</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas urna risus, vitae condimentum mi hendrerit ac. Mauris tempus mi a ante sollicitudin, commodo tincidunt libero sagittis. Nullam ornare blandit pharetra Phasellus dictum aliquet purus sit amet faucibus.</h2>
            </div><!-- end col -->
            <div class="col-sm-3">
                <a href=""><img src="images/more_about_us.png" class="img-responsive" alt=""></a>
            </div><!-- end col -->
        </div>
    </div>
</div>

<!-- road skills services
============================================================= -->
<div class="service-details">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="images/FORS_home.png" class="img-responsive" alt="fors view">
                <h2>FORS</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus pulvinar nisl et blandit. Curabitur sed commodo odio. Nulla mauris nulla, ullamcorper nec dlit ut, viverra sodales est. Curabitur pellentesque euismod justo aucibus pulvinar nisl et... <a href="#">more</a></p>
                <a href="#"><img src="images/read_more.png" class="img-responsive" alt="Read more >"></a>
            </div><!-- end col -->
            <div class="col-sm-3">
                <img src="images/audits_home.png" class="img-responsive" alt="audits">
                <h2>Audits</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus pulvinar nisl et blandit. Curabitur sed commodo odio. Nulla mauris nulla, ullamcorper nec dlit ut, viverra sodales est. Curabitur pellentesque euismod justo aucibus pulvinar nisl et... <a href="#">more</a></p>
                <a href="#"><img src="images/read_more.png" class="img-responsive" alt="Read more >"></a>
            </div><!-- end col -->
            <div class="col-sm-3">
                <img src="images/driver_training_home.png" class="img-responsive" alt="driver training">
                <h2>Driver Training</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus pulvinar nisl et blandit. Curabitur sed commodo odio. Nulla mauris nulla, ullamcorper nec dlit ut, viverra sodales est. Curabitur pellentesque euismod justo aucibus pulvinar nisl et... <a href="#">more</a></p>
                <a href="#"><img src="images/read_more.png" alt="Read more >"></a>
            </div><!-- end col -->
            <div class="col-sm-3">
                <img src="images/telematics_home.png" class="img-responsive" alt="telematics">
                <h2>Telematics</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus pulvinar nisl et blandit. Curabitur sed commodo odio. Nulla mauris nulla, ullamcorper nec dlit ut, viverra sodales est. Curabitur pellentesque euismod justo aucibus pulvinar nisl et... <a href="#">more</a></p>
                <a href="#"><img src="images/read_more.png" class="img-responsive" alt="Read more >"></a>
            </div>
        </div>
    </div>
</div>
<!-- serviec text and item
=============================================================== -->
<div class="service">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-offset-2">
                <h2>Services Text</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas urna risus, vitae condimentum mi hendrerit ac. Mauris tempus mi a ante sollicitudin, commodo tincidunt libero sagittis. Nullam ornare blandit pharetra. Mauris ut ante metus. Phasellus dictum aliquet purus sit amet faucibus. Vestibulum vulputate diam in magna lacinia porta. Etiam luctus eget nulla quis egestas Aliquam vulputate, lacus eget porttitor malesuasa, lorem sapien efficitur mauris, quis sodales enim est nec tellus</p>
            </div>
        </div><!-- end row -->
        <div class="row service-item">
            <div class="col-sm-2">
                <img src="images/reduce_insurance.svg.png" class="img-responsive" alt="Reduce">
                <h3>Reduce Insurance Premiums</h3>
                <p>If you reduce your insurance premiums by nearly 50%</p>
            </div><!-- end col -->
            <div class="col-sm-2">
                <img src="images/improve_mpg.svg.png" class="img-responsive" alt="Improve">
                <h3>Improve MPG</h3>
                <p>Cutting your transport-related costs can make a huge difference to your bottom line.</p>
            </div><!-- end col -->
            <div class="col-sm-2">
                <img src="images/audits_risk.svg.png" class="img-responsive" alt="audits">
                <h3>Audits & Risk Assessments</h3>
                <p>Reduce your costs, improve your safety record or become legally compliant.</p>
            </div><!-- end col -->
            <div class="col-sm-2">
                <img src="images/fors.svg.png" class="img-responsive" alt="Fors">
                <h3>FORS</h3>
                <p>If you are looking to gain FORS recognition, this information is for you.</p>
            </div><!-- end col -->
            <div class="col-sm-2">
                <img src="images/o_licence.svg.png" class="img-responsive" alt="'O'Licence">
                <h3>'O' Licence Undertaking</h3>
                <p>Every haulage operator undertakes to have Porper Arrangements...</p>
            </div><!-- end col -->
            <div class="col-sm-2">
                <img src="images/collision_prevention.svg.png" class="img-responsive" alt="Collision">
                <h3>Collision Prevention</h3>
                <p>Collision damage business and your image...</p>
            </div><!-- end col -->
            <!-- ---- demo div for slide-------------------- -->
            <div class="col-sm-2">
                <img src="images/improve_mpg.svg.png" class="img-responsive" alt="Improve">
                <h3>Improve MPG</h3>
                <p>Cutting your transport-related costs can make a huge difference to your bottom line.</p>
            </div><!-- end col -->
        </div><!-- end row -->
    </div>
</div>


<!-- org testimonial
============================================================================ -->
<div class="org-testimonial">
    <div class="container testimonial-slide">
        <div class="row">
            <div class="col-sm-3">
                <img src="images/testimonial_dummy.jpg" class="img-responsive" alt="">
            </div><!-- end col -->
            <div class="col-sm-8">
                <h3><i>"The key element of OPCOM is that it has helped to establish important systems and procedures within our business. We have become more active in reducing our exposure to risk. Building on this has allowed us to address business issues such as improving MPG.</i></h3>
                <h3><i>OPCOM has provided us with the complete package to enable us to reduce risk, remain legally compliant and help increase efficiency."</i></h3>
                <br><br>
                <span>Simon Chamberlain, Managing Dircetor</span><br>
                <span><b>Chamberlain Transport Ltd</b></span>
            </div><!-- end col -->
        </div>
        <!-- just a demo div for slide -->
        <div class="row">
            <div class="col-sm-3">
                <img src="images/testimonial_dummy.jpg" alt="">
            </div>
            <div class="col-sm-8">
                <h3>Road Skill Lorem ipsum..........</h3>
                <h3>demo text</h3>
                <br><br>
                <span>Loirm ipsum</span><br>
                <span>Loirm ipsum</span>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
