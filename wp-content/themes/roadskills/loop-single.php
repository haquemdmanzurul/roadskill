 <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?> 

 <div class="col-sm-12">
                  
                  <?php the_post_thumbnail(); ?>
                  <h2><?php the_title(); ?></h2>
                  <p><?php the_content(); ?></p>
                  
                  <hr>
                  <span><?php the_date() ?>-<?php the_time() ?></span>
              </div><!-- end col --> 

<?php endwhile; ?> 
<?php else : ?> 
<h3><?php _e('404 Error&#58; Not Found', 'zahantech'); ?></h3> 
<?php endif; ?>