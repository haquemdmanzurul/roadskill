 <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?> 

 <div class="blog-item col-sm-4">
                  
                  <?php the_post_thumbnail( 'post', array( 'class' => 'img-responsive' ) ); ?>
                  <h2><?php the_title(); ?></h2>
                  <p><?php the_excerpt(); ?></p>
                  <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/read_more.png" alt="Read more"></a>
                  <hr>
                  <span><?php the_date() ?>-<?php the_time() ?></span>
              </div><!-- end col --> 

<?php endwhile; ?> 
<?php else : ?> 
<h3><?php _e('404 Error&#58; Not Found', 'zahantech'); ?></h3> 
<?php endif; ?>