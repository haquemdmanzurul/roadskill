<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <!-- Blog banner
  ======================================================================= -->
  <div class="blog-banner">
      <div class="container">
          <div class="row">
              <h1><?php the_title( ); ?></h1>
          </div>
      </div>
  </div>
  
  <!-- Blog title
  ===================================================================== -->
  <div class="blog-title">
      <div class="container">
          <div class="row">
              <div class="col-sm-8 col-md-offset-2">
                  <h2><b>Roadskills</b> BLOG</h2>
                  <h3>Click the articles on the left to find out read our latest BLOG articles</h3>
              </div>
          </div>
      </div>
  </div>
  
  <!-- blog item in row
  ================================================================= -->
  <div class="blog-itemA">
      <div class="container">
          <div class="row">
          
          
     
          
          
          
         
          
          <?php
				 
				if (have_posts()) : while (have_posts()) : the_post(); ?>      
				 <div class="blog-item col-sm-4 cat-con">
                  <a href="<?php the_permalink() ?>" class="postimg"><?php the_post_thumbnail( $size, $attr ); ?> </a>
                  <h2> <?php the_title();?></h2>
                   <?php the_excerpt();?>
                   <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/read_more.png" alt="Read more"></a>
                 <hr>
                  <span><?php echo get_the_date(); ?> <?php echo get_the_time(); ?>  </span>
                </div><!-- end col -->
               <?php endwhile; else: ?>
				<?php _e('No Posts Sorry Enter Your Currect Category Id.'); ?>
				<?php endif; ?>
          
          
           
          </div>
      </div>
  </div>
  
  
 
   
  <?php get_template_part('testimonial'); ?>			 

<?php
get_footer();
?>

