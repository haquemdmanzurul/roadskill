<div class="service">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-md-offset-2">
                   <h2>Services Text</h2>
                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas urna risus, vitae condimentum mi hendrerit ac. Mauris tempus mi a ante sollicitudin, commodo tincidunt libero sagittis. Nullam ornare blandit pharetra. Mauris ut ante metus. Phasellus dictum aliquet purus sit amet faucibus. Vestibulum vulputate diam in magna lacinia porta. Etiam luctus eget nulla quis egestas Aliquam vulputate, lacus eget porttitor malesuasa, lorem sapien efficitur mauris, quis sodales enim est nec tellus</p>
               </div>
           </div><!-- end row -->
           <div class="row service-item">
           
           <?php
				global $post;
				$args = array( 'posts_per_page' => -1, 'post_type'=> 'services' );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) : setup_postdata($post); ?>
					
                    <div class="col-sm-2">
                   <?php the_post_thumbnail( 'services', array( 'class' => 'img-responsive' ) ); ?>
                   <h3><?php the_title(); ?></h3>
                   <p><?php the_content(); ?></p>
               </div><!-- end col -->
                    
				<?php endforeach; ?>
               
              
           </div><!-- end row -->
       </div>
   </div>