 <!-- org testimonial
   ============================================================================ -->
   <div class="org-testimonial">
       <div class="container testimonial-slide">
       
       <?php
				global $post;
				$args = array( 'posts_per_page' => -1, 'post_type'=> 'testimonial' );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) : setup_postdata($post); ?>
					
                    <div class="row">
               <div class="col-sm-3">
                   <?php the_post_thumbnail( 'testimonial', array( 'class' => 'img-responsive' ) ); ?>
               </div><!-- end col -->
               <div class="col-sm-8">
                   <h3><i><?php the_title(); ?></i></h3>
                   <span><b><?php the_content(); ?></b></span>
               </div><!-- end col -->
           </div>
                    
				<?php endforeach; ?>
           
    
       </div>
   </div>