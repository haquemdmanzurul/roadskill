<?php
    wp_reset_query();
    $args = array( 'post_type' => 'slider', 'posts_per_page' => 10 );
    $loop = new WP_Query( $args );
    $slides = "";
    $indicators = "";
    $i = 0;
    while ( $loop->have_posts() ) : $loop->the_post();
        $image = get_field('slide_image');
        $title = get_the_title();
        $content = get_the_content();
        $indicators .= '<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="active"></li>';

        if($i == 0)
            $active = 'active';
        else
            $active = '';
        $slides .= '<div class="item '.$active.'">
              <img src="'.$image.'"/>
              <div class="carousel-caption">
                  <h1>'.$title.'</h1>
                  '.$content.'
              </div>
            </div>';
        $i++;
     endwhile;
?>

<div id="carousel-example-generic" class="carousel slide " data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
          <?php
          echo $indicators;
          ?>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">

          <?php
          echo $slides;
          ?>

      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>