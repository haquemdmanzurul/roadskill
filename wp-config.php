<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'roadskills2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PhgWkS;#@wwGCtk[-4F]g$o 8$6=#uBqI0]);PsF3T?ur:;*oT%eDu#U<-YvVJjB');
define('SECURE_AUTH_KEY',  'fC2ZWH-J1305Fhc_ZdvrbT}/%xh)qI#qomwI_Er:65#?K;~?b^hHFvh-HLDTqws;');
define('LOGGED_IN_KEY',    '|1)2}W:&+.Pytjp 8VZ+nBc#{G)HZf[E]U#XveVVFUC+!TL`!]}/i3CdL;uUrC:g');
define('NONCE_KEY',        '|{Hhb&;<;3lS(5~T<(-{!R^h$b&h.~[#Gk!}WrH$}o+XLZsvl{KH,ucd[FePM_oV');
define('AUTH_SALT',        'D-O6TGfnAL7o|~^P4SU<TC<],$m)V6=uK|+J-a]Zg>4*m?V-?V1QgA&fHl-u] -b');
define('SECURE_AUTH_SALT', 'fCx)tt+5LDB2 ^*y(28Fh_#-O=/<n4OIBy:LlZXkp)RY%!d:O vrT-fD9lN4|(Q=');
define('LOGGED_IN_SALT',   '8aN0:F:er,$e9j9b/ikBw7?Yd5dl#/P+Uv2ZKO=h+/4(T=9wp^J@nT1}s[=C!Val');
define('NONCE_SALT',       'b>ps}{o3cbd|#Sb304>UjTgg}!00vYbW8ym)|^UV4R[/L+M|SNaffpqI+[6aJ:uM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
